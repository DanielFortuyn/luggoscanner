require("dotenv").config();

import dotenv from 'dotenv';
import axios from 'axios';
import { readline } from 'readline';
import fs from 'fs';
import Jimp from 'jimp';
import jsQR from 'jsqr';

import * as keyTable from './keytable.js';
import * as log from './log.js';

const sampleData = require("./luggage.json");

dotenv.config();
let stringArray = [];

readline.emitKeypressEvents(process.stdin);

if (process.stdin.isTTY) {
  process.stdin.setRawMode(true);
}

const endpoint =
  process.env.LUGGO_ENDPOINT ||
  "https://mockbin.org/bin/f8213285-3076-4464-9074-7ea1a6acee37";
const deviceLocation = process.env.OUTPUT_DEVICE || "/dev/hidg0";

let jwt = {
  ttl: 0,
  token: "",
};

log("Starting the app..");
//Listen for keys
process.stdin.on("keypress", (_str, _key) => {
  //Exit on ctrl+c
  if (_key && _key.ctrl && _key.name == "c") process.exit();
  //Push _key to array until return
  if (_key.shift) {
    stringArray.push(_str.toUpperCase());
  } else {
    stringArray.push(_str);
  }
  if (_key.name == "return") {
    log("Submitting: " + stringArray.join(""));
    handleSubmit(stringArray.join(""));
    log("Done");
    stringArray = [];
  }
});

async function authenticate() {
  const luggoPassword = process.env.LUGGO_PASSWORD || "test";
  const luggoUsername = process.env.LUGGO_USERNAME || "test";

  return await axios.post(endpoint + "/login", {
    username: luggoUsername,
    password: luggoPassword,
  });
}

async function getRequestHeaders() {
  const now = Date.now();

  if (jwt.ttl < now) {
    log("Authenticating..");
    let auth = authenticate();
    // Fix when details available
    jwt.ttl = now + 10000;
    jwt.token = "uuddlrlrbabab";
    log(jwt);
  }
  return {
    headers: {
      Authenticate: `Bearer ${jwt.token} `,
    },
  };
}

async function handleSubmit(_sealcode) {
  let headers = await getRequestHeaders();

  axios.post(endpoint, { sealcode: _sealcode }, headers).then((res) => {
    if (typeof res.data?.qr != "undefined") {
      const { qr } = res.data;
      log(`Received QR, decoding..`);
      const data = decodeQrToData(qr);

      let writer = fs.createWriteStream(deviceLocation);
      for (let i = 0; i < data.length; i++) {
        writeKey(writer, data[i]);
      }
      writer.write("\0\0\x28\0\0\0\0\0");
      writer.write("\0\0\0\0\0\0\0\0");
      writer.close();
    }
  });
}

function decodeQrToData(qr) {
  const buffer = Buffer.from(qr, 'base64');
  return Jimp.read(buffer, async function(err, image) {
    if(err) {
      throw err;
    }
    const { data } = jsQR(image.bitmap.data, image.bitmap.width, image.bitmap.height);
    return data;
  });
}

const UClist = [":", "{", "}", '"'];

function writeKey(_writer, _letter) {
  const hidKey = keyTable[_letter.toLowerCase()];
  if (hidKey) {
    if (UClist.indexOf(_letter) !== -1 || _letter != _letter.toLowerCase()) {
      log("Writing UC.. " + _letter);
      _writer.write("\x20\0" + hidKey + "\0\0\0\0\0");
    } else {
      log("Writing LC.. " + _letter);
      _writer.write("\0\0" + hidKey + "\0\0\0\0\0");
    }
    //Release all keys
    _writer.write("\0\0\0\0\0\0\0\0");
  } else {
    console.log("Not writing cus unknown.. " + _letter);
  }
}
