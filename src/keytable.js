export default {
        // 3: "\x48",  // Pause / Break
        // 8: "\x2a",  // Backspace / Delete
        // 9: "\x2b",  // Tab
        // 12: "\x53",  // Clear
        // "\r": "\x28",  // Enter
        // 16: "\xe1",  // Shift (Left)
        // 17: "\xe0",  // Ctrl (left)
        // 18: "\xe1",  // Alt (left)
        // 19: "\x48",  // Pause / Break
        // 20: "\x39",  // Caps Lock
        // 21: "\x90",  // Hangeul
        // 25: "\x91",  // Hanja
        // 27: "\x29",  // Escape
        // 32: "\x2c",  // Spacebar
        // 33: "\x4b",  // Page Up
        // 34: "\x4e",  // Page Down
        // 35: "\x4d",  // End
        // 36: "\x4a",  // Home
        // 37: "\x50",  // Left Arrow
        // 38: "\x52",  // Up Arrow
        // 39: "\x4f",  // Right Arrow
        // 40: "\x51",  // Down Arrow
        // 41: "\x77",  // Select
        // 43: "\x74",  // Execute
        // 44: "\x46",  // Print Screen
        // 45: "\x49",  // Insert
        // 46: "\x4c",  // Delete
        // 47: "\x75",  // Help
        0: "\x27",  // 0
        1: "\x1e",  // 1
        2: "\x1f",  // 2
        3: "\x20",  // 3
        4: "\x21",  // 4
        5: "\x22",  // 5
        6: "\x23",  // 6
        7: "\x24",  // 7
        8: "\x25",  // 8
        9: "\x26",  // 9
        a: "\x04",  // a
        b: "\x05",  // b
        c: "\x06",  // c
        d: "\x07",  // d
        e: "\x08",  // e
        f: "\x09",  // f
        g: "\x0a",  // g
        h: "\x0b",  // h
        i: "\x0c",  // i
        j: "\x0d",  // j
        k: "\x0e",  // k
        l: "\x0f",  // l
        m: "\x10",  // m
        n: "\x11",  // n
        o: "\x12",  // o
        p: "\x13",  // p
        q: "\x14",  // q
        r: "\x15",  // r
        s: "\x16",  // s
        t: "\x17",  // t
        u: "\x18",  // u
        v: "\x19",  // v
        w: "\x1a",  // w
        x: "\x1b",  // x
        y: "\x1c",  // y
        z: "\x1d",  // z
        // 91: "\xe3",  // Windows key / Meta Key (Left)
        // 96: "\x62",  // Numpad 0
        // 97: "\x59",  // Numpad 1
        // 98: "\x5a",  // Numpad 2
        // 99: "\x5b",  // Numpad 3
        // 100: "\x5c",  // Numpad 4
        // 101: "\x5d",  // Numpad 5
        // 102: "\x5e",  // Numpad 6
        // 103: "\x5f",  // Numpad 7
        // 104: "\x60",  // Numpad 8
        // 105: "\x61",  // Numpad 9
        // 112: "\x3b",  // F1
        // 113: "\x3c",  // F2
        // 114: "\x3d",  // F3
        // 115: "\x3e",  // F4
        // 116: "\x3f",  // F5
        // 117: "\x40",  // F6
        // 118: "\x41",  // F7
        // 119: "\x42",  // F8
        // 120: "\x43",  // F9
        // 121: "\x44",  // F10
        // 122: "\x45",  // F11
        // 123: "\x46",  // F12
        // 124: "\x68",  // F13
        // 125: "\x69",  // F14
        // 126: "\x6a",  // F15
        // 127: "\x6b",  // F16
        // 128: "\x6c",  // F17
        // 129: "\x6d",  // F18
        // 130: "\x6e",  // F19
        // 131: "\x6f",  // F20
        // 132: "\x70",  // F21
        // 133: "\x71",  // F22
        // 134: "\x72",  // F23
        // 144: "\x53",  // Num Lock
        // 145: "\x47",  // Scroll Lock
        // 161: "\x1e",  // !
        // 163: "\x32",  // Hash
        // 173: "\x2d",  // Minus
        // 179: "\xe8",  // Media play/pause
        // 168: "\xfa",  // Refresh
        ":": "\x33",  // Semicolon
        "=": "\x2e",  // Equal sign
        ",": "\x36",  // Comma
        // 189: "\x2d",  // Minus sign
        // 190: "\x37",  // Period
        "\\": "\x31",  // Forward slash
        // 192: "\x35",  // Accent grave
        "{": "\x2f",  // Left bracket ([, {])
        "/": "\x38",  // Back slash
        "+": "\x57",
        "}": "\x30",  // Right bracket (], })
        "\"": "\x34",  // Single quote
        // 223: "\x35",  // Accent grave (`)
}