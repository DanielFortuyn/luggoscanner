import PDFDocument from 'pdfkit';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import { exec } from 'node:child_process';
import QRCode from 'qrcode'


async function main() {
    console.log('Hit main..');
    const startY = 20;
    try {
        const doc = new PDFDocument({
            size: [425, 200]
        });


        let str = [];
        for (let i = 0; i < 7; i++) {
            str.push(Math.floor(Math.random() * 10).toString());
        }


        const writeStream = fs.createWriteStream('test.pdf');
        doc.pipe(writeStream);
        doc.save()

        doc.rotate(90, { origin: [0, 0] });
        doc.image('l.png', 150, 150, {
            fit: [150, 50],
            align: 'center',
            valign: 'center'
        });
        doc.restore()

        const text = `https://luggo.com?code=${str.join('')}uuid=danieltestqrcodes`;
        const qr = await QRCode.toDataURL(text, { margin: 0 });
        console.log(qr);
        let qrpts = qr.split(",");
        let img = Buffer.from(qrpts[1], 'base64url');
        console.log(qrpts[1]);

        doc.image(img, 55, startY);

        doc.fontSize(30).text('TESTLABELLUGGO', 190, startY);
        doc.fontSize(12).text(str.join(''), 210, startY);

        doc.end();
        const __filename = fileURLToPath(import.meta.url);
        const pdfPath = path.dirname(__filename) + "/../test.pdf";
        console.log(pdfPath);

        writeStream.on('finish', (result) => {
            exec(`lp -d Custom2 ${pdfPath}`);
        });
    } catch (e) {
        console.log(e);
    }
}

(async () => {
    await main();
})();